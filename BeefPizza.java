package structuralPattern;

public class BeefPizza extends  FoodDecorator{

	public BeefPizza(Pizza newPizza) {  
        super(newPizza);  
    }  
    public String preparePizza(){  
        return super.preparePizza() +" With Roasted Beef and Beef Curry  ";   
    }  
    public double foodPrice()   {  
        return super.foodPrice()+1550.0;  
    } 


}
