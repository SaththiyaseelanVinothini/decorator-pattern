package structuralPattern;

public abstract class FoodDecorator implements Pizza{  
    private Pizza newPizza;  
    public FoodDecorator(Pizza newPizza)  {  
        this.newPizza=newPizza;  
    }  
    @Override  
    public String preparePizza(){  
        return newPizza.preparePizza();   
    }  
    public double foodPrice(){  
        return newPizza.foodPrice();  
    }  
} 
