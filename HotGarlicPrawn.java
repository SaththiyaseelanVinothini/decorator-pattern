package structuralPattern;

public class HotGarlicPrawn extends  FoodDecorator {

	public HotGarlicPrawn(Pizza newPizza) {  
        super(newPizza);  
    }  
    public String preparePizza(){  
        return super.preparePizza() +" With Roasted HotGarlic and Prawn Curry  ";   
    }  
    public double foodPrice()   {  
        return super.foodPrice()+1350.0;  
    } 

}
