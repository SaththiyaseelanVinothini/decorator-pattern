package structuralPattern;

public interface Pizza {
	 public String preparePizza();  
	    public double foodPrice();  
}
