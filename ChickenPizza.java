package structuralPattern;

public class ChickenPizza extends  FoodDecorator{

	  public ChickenPizza(Pizza newPizza) {  
	        super(newPizza);  
	    }  
	    public String preparePizza(){  
	        return super.preparePizza() +" With Roasted Chiken and Chiken Curry  ";   
	    }  
	    public double foodPrice()   {  
	        return super.foodPrice()+1150.0;  
	    } 

}
