package structuralPattern;

import java.io.BufferedReader;  
import java.io.IOException;  
import java.io.InputStreamReader;  
public class DecoratorPatternCustomer {  
    private static int  choice;  
    public static void main(String args[]) throws NumberFormatException, IOException    {  
       do{        
        System.out.print("========= Pizza Menu ============ \n");  
        System.out.print("            1. Vegetarian Pizza.   \n");  
        System.out.print("            2. Chicken Pizza.       \n");  
        System.out.print("            3. Beef Pizza.           \n");  
        System.out.print("            4. Hot Garli Prawn Pizza   \n");
        System.out.print("            5. Exit                    \n");  
        System.out.print("Enter your choice: ");  
        BufferedReader br=new BufferedReader(new InputStreamReader(System.in));   
        choice=Integer.parseInt(br.readLine());  
        switch (choice) {  
        case 1:{   
                 VegPizza vf=new VegPizza();  
              System.out.println(vf.preparePizza());  
              System.out.println( vf.foodPrice());  
            }  
            break;  
              
                case 2:{  
                	 Pizza f1=new ChickenPizza((Pizza) new VegPizza() );  
                    System.out.println(f1.preparePizza());  
                System.out.println( f1.foodPrice());  
        }  
            break;    
     case 3:{  
    	 Pizza f2=new BeefPizza((Pizza) new VegPizza());  
                     System.out.println(f2.preparePizza());  
                    System.out.println( f2.foodPrice());  
              }  
            break; 
     case 4:{  
         Pizza f3=new HotGarlicPrawn((Pizza) new VegPizza());  
                 System.out.println(f3.preparePizza());  
                System.out.println( f3.foodPrice());  
          }  
        break;
              
         default:{    
            System.out.println("Other than these no food available");  
        }         
    return;  
     }//end of switch  
          
}while(choice!=5);  
    }  
} 
